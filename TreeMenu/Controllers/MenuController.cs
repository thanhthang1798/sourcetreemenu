using Data.DBEntities;
using Microsoft.AspNetCore.Mvc;
using TreeMenu.Business.Services;
using TreeMenu.Models;

namespace TreeMenu.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MenuController : ControllerBase
    {
        #region Varibal
        readonly IMenuService _menuService;
        #endregion

        public MenuController(IMenuService menuService)
        {
            this._menuService = menuService;
        }


        [HttpGet(Name = "GetList")]
        public List<MenuDTO> Get()
        {
            var data = _menuService.GetData();
            return data; 
        }

        [HttpPost(Name = "Menu")]
        public bool Post([FromBody]ReqAdd menu)
        {
            menu.IsNew = menu.Rowid == 0;
            var data = _menuService.Save(menu,menu.IsNew);
            return data; 
        }
        [HttpDelete(Name = "Menu")]
        public bool Del(int id)
        {
            var data = _menuService.Delete(id);
            return data; 
        }
    }
    public class ReqAdd: Menu
    { 
        public bool IsNew { get; set; }
    }
}
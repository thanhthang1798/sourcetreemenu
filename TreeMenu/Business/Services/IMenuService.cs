﻿using Data.DBEntities;
using System.Diagnostics.Metrics;
using TreeMenu.Models;

namespace TreeMenu.Business.Services
{
    public interface IMenuService
    {
        List<MenuDTO> GetData();
        bool Save(Menu rowData, bool isNew);

        bool Delete(int id);
    }
}

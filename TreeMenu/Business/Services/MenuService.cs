﻿using Data;
using Data.DBEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TreeMenu.Models;

namespace TreeMenu.Business.Services
{
    public class MenuService : Repository<Menu>, IMenuService
    {
        public List<MenuDTO> GetData()
        {
            Func<Menu, bool> func = f => f.IsDel == false || f.IsDel == null;
            var data = GetList(func);

            var newdata = from d in data.ToList() 
                          where d.Levelid == 1
                          select new MenuDTO
                          {
                              Rowid = d.Rowid,
                              Name = d.Name,
                              Levelid = d.Levelid,
                              Parentid = d.Parentid,
                              child = GetChild(data.ToList(), d.Rowid)
                          };

            return newdata.ToList();
        }
        private List<MenuDTO> GetChild(List<Menu> menu, int parentid)
        {
            var data = from d in menu.ToList()
                       where d.Parentid == parentid
                       select new MenuDTO
                       {
                           Rowid = d.Rowid,
                           Name = d.Name,
                           Levelid = d.Levelid,
                           Parentid = d.Parentid,
                           child = GetChild(menu.ToList(), d.Rowid)
                       };
            return data.ToList();
        }
        public bool Save(Menu rowData, bool isNew)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    if (isNew)
                    {
                        db.Menus.Add(rowData);
                    }
                    else
                    {
                        db.Entry(rowData).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (var db = new DBEntities())
                {
                    //TODO LIST: Kiểm tra sử dụng trước khi xóa

                    Func<Menu, bool> func = f => f.IsDel == false && f.Rowid == id;
                    var data = GetList(func);
                    if (data == null || !data.Any()) return false;
                    Menu menu = data.FirstOrDefault();
                    menu.IsDel = true;
                    db.Entry(menu).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

﻿using Data.DBEntities;

namespace TreeMenu.Models
{
    public class MenuDTO:Menu
    {
        public List<MenuDTO> child { get; set; }
    }
}

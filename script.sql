USE [TreeMenu]
GO
/****** Object:  Table [dbo].[Menu]    Script Date: 5/19/2023 9:44:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menu](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](500) NULL,
	[levelid] [int] NULL,
	[parentid] [int] NULL,
	[isDel] [bit] NULL,
 CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED 
(
	[rowid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Menu] ADD  CONSTRAINT [DF_Menu_isDel]  DEFAULT ((0)) FOR [isDel]
GO

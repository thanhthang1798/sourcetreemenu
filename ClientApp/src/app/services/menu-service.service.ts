import { Injectable } from '@angular/core';
import { ROOT_URL } from 'src/environments/environment.development';
import { HttpClient } from '@angular/common/http';
import { Menu } from '../models/Menu';

@Injectable({
  providedIn: 'root'
})
export class MenuServiceService {

  private baseUrl = ROOT_URL;

  constructor(public http: HttpClient) {

   }

  getListMenu(){
    return this.http.get<Menu[]>(this.baseUrl+"Menu");
  }


  AddNewItem(data:any){
    return this.http.post<any>(this.baseUrl+"Menu",data);
  }

  DelItem(id:number){
    return this.http.delete<any>(this.baseUrl+"Menu?id="+id);
  }
}

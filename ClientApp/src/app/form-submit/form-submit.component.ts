import { Component,Input, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Menu } from '../models/Menu';

@Component({
  selector: 'app-form-submit',
  templateUrl: './form-submit.component.html',
  styleUrls: ['./form-submit.component.scss']
})
export class FormSubmitComponent implements OnInit {

  form = new FormGroup({
    name: new FormControl('', Validators.minLength(2)),
    rowid: new FormControl(0),
    levelid: new FormControl(1),
    parentid: new FormControl(0),
  });
  @Input() parentid : number = 0;
  @Input() levelid : number = 1;
  @Input() rowid : number = 0;
  @Input() name : string;
  @Output() menu: EventEmitter<any> = new EventEmitter();

  onSubmit(): void {
    this.menu.emit(this.form.value);
  }

  ngOnInit(): void {
    this.builDataForm();
  }

  builDataForm(){
    this.form = new FormGroup({
      name: new FormControl(this.name),
      rowid: new FormControl(this.rowid),
      levelid: new FormControl(this.levelid),
      parentid: new FormControl(this.parentid),
    });
  }
}

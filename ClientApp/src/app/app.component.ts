import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MenuServiceService } from './services/menu-service.service';
import { Observable } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Menu } from './models/Menu';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'ClientApp';
  public listMenu: any;
  public add : boolean = false;

  constructor(public menuS: MenuServiceService) {}

  ngOnInit(): void {
    this.LoadData();
  }

  LoadData() {
    this.menuS.getListMenu().subscribe((data: Menu[]) => {
      this.listMenu = data;
    });
    this.add = false;
  }

  toggleClick(ele: any) {
    ele.toggle = !ele.toggle;
  }
  SubmitData(data: any): void {
    this.AddNewItem(data);
  }

  AddNewItem(item: Menu) {
    this.menuS.AddNewItem(item).subscribe((data: any) => {
      this.LoadData();
    });
  }
  DelItem(id: number) {
    this.menuS.DelItem(id).subscribe((data: any) => {
      if(data){
        alert("Xóa thành công");
      }
      this.LoadData();
    });
  }
}

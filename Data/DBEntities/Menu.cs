﻿using System;
using System.Collections.Generic;

namespace Data.DBEntities;

public partial class Menu
{
    public int Rowid { get; set; }

    public string? Name { get; set; }

    public int? Levelid { get; set; }

    public int? Parentid { get; set; }

    public bool? IsDel { get; set; }
}

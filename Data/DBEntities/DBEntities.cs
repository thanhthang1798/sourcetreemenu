﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Data.DBEntities;

public partial class DBEntities : DbContext
{
    public DBEntities()
    {
    }

    public DBEntities(DbContextOptions<DBEntities> options)
        : base(options)
    {
    }

    public virtual DbSet<Menu> Menus { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=166469-NTTHANG\\THANHTHANG;Database=TreeMenu;User ID=sa;Password=123;TrustServerCertificate=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Menu>(entity =>
        {
            entity.HasKey(e => e.Rowid);

            entity.ToTable("Menu");

            entity.Property(e => e.Rowid).HasColumnName("rowid");
            entity.Property(e => e.IsDel)
                .HasDefaultValueSql("((0))")
                .HasColumnName("isDel");
            entity.Property(e => e.Levelid).HasColumnName("levelid");
            entity.Property(e => e.Name)
                .HasMaxLength(500)
                .HasColumnName("name");
            entity.Property(e => e.Parentid).HasColumnName("parentid");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}

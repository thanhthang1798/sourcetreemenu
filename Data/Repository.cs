﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore.SqlServer.Storage.Internal;

namespace Data
{
    public class Repository<T> where T : class, new()
    {

        public IList<T> GetList(Func<T, bool> where  )
        {
            try
            {
                using (var db = new DBEntities.DBEntities())
                {
                    List<T> list;
                    IQueryable<T> dbQuery = db.Set<T>();

                    list = dbQuery
                        .AsNoTracking()
                        .Where(where)
                        .ToList<T>();
                    return list;
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
